import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: any;
  menuDev = false;
  userId: string;
  constructor(service: ServiceService, private active: ActivatedRoute) {
    this.active.params.subscribe(params => this.userId = params.gitName);
    service.getUser(this.userId).subscribe(user => {
      this.user = user;
      console.log(user);
      console.log(typeof user);
    });
  }

  ngOnInit() {
  }



}

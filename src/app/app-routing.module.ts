import { Router, Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { ModuleWithProviders } from '@angular/core';
import { ProfileComponent } from './profile/profile.component';
import { DevsComponent } from './devs/devs.component';

const APP_ROUTES: Routes = [
  { path: '', component: AuthComponent },
  { path: 'profile/:gitName', component: ProfileComponent },
  { path: 'devs', component: DevsComponent }
]

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);

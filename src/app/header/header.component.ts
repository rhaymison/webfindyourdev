import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() menuDev = new EventEmitter();
  @Output() profile = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  feedBack() {
    this.menuDev.emit({
      dev: true,
    });
  }

  myProfile() {
    this.menuDev.emit({
      dev: false,
    });
  }
}

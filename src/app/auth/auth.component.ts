import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceService } from '../service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  erro: string = '';
  loginForm: FormGroup;
  @ViewChild('nameUserInput', { static: true }) userName: ElementRef<HTMLInputElement>;

  constructor(private formBuilder: FormBuilder, private service: ServiceService, private route: Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login() {
    const email = this.loginForm.get('email').value;
    const password = this.loginForm.get('password').value;
    this.service.login(email, password).subscribe(
      data => {
        if (data === 203) {
          this.erro = 'Email ou senha inválidos';
        } else {
          this.route.navigate(['profile', data.git]);
        }
      }
      , err => {
        console.log(err);
        this.loginForm.reset();
        this.userName.nativeElement.focus();
      }
    )
  }

}

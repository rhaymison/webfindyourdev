import { Component, OnInit, Input, Output } from '@angular/core';
import { TouchSequence } from 'selenium-webdriver';
import { DevModel } from './dev.model';

@Component({
  selector: 'app-devs',
  templateUrl: './devs.component.html',
  styleUrls: ['./devs.component.css']
})
export class DevsComponent implements OnInit {
  dev: DevModel;
  key = false;


  constructor() { }

  ngOnInit() {
  }

  onSearchDev(dev: HTMLInputElement) {
    const devName = this.getDev(dev.value);
    devName.then(dados => {
      this.showDev(dados);
    });
  }

  async getDev(dev: string) {
    const url = 'https://api.github.com/users/' + dev;
    const resp = await fetch(url);
    if (!resp.ok) { throw new Error('devInvalido'); }
    const json = await resp.json();
    return json;
  }

  showDev(dados: any) {
    if (dados) {
      this.key = true;
      this.dev = dados;
      this.dev = new DevModel(dados.name, dados.login, dados.avatar_url, dados.bio, dados.html_url);

    }
  }
}

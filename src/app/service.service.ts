import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
const API = 'http://localhost:8000/api';

@Injectable({
  providedIn: 'root'
})

export class ServiceService {

  constructor(private http: HttpClient) { }

  getUser(userName: string) {
    return this.http.get(API + '/usuario/' + userName);
  }

  login(email: string, password: string) {
    return this.http.post(API + '/login', { email, password });
  }
}

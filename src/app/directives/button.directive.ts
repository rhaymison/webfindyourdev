import { Directive, HostBinding, HostListener, ElementRef, Renderer2, OnInit } from '@angular/core';
import { Event } from '@angular/router';

@Directive({
  selector: '[appButton]'
})
export class ButtonDirective implements OnInit {
  @HostBinding('style.backgroundColor') backgroundColor: string = '#0275d8';

  constructor(private elRef: ElementRef, private render: Renderer2) { }

  ngOnInit() {

  }

  @HostListener('click') mouseclick(eventData: Event) {
    this.backgroundColor = '#d9534f';
  }

  @HostListener('mouseleave') mouseleave(eventData: Event) {
    this.backgroundColor = '#0275d8';
  }
}
